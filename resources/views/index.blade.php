<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>window.Laravel = { csrfToken: '{{ csrf_token() }}' }</script>

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Parking</title>

    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="{{ asset('css/parkingSystem.css') }}" rel="stylesheet" >
</head>
<body>

    <div id="app">
        <nav>
            <ul>
                <li><a href="">Francisco</a></li>
                <li style="float:right">
                    <a href="#">
                        <button class="btn-add-vehicle shadow" id="newVehicle">Add Car</button>
                    </a>
                </li>
                <li style="float:right">
                    <a href="#">
                        <button class="btn-add-vehicle shadow" id="exitVehicle">Exit Car</button>
                    </a>
                </li>
            </ul>
        </nav>
        <index-component></index-component>
    </div>
    <script src="{{ asset('js/parkingSystem.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
