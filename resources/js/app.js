require('./bootstrap');

window.Vue = require('vue');
window.swal = require('sweetalert');
// import Vuelidate from 'vuelidate';
// Vue.use(Vuelidate);

// import VueRouter from 'vue-router';
// Vue.use(VueRouter);

import VueAxios from 'vue-axios';
import axios from 'axios';
// import base_64 from 'base-64';
window.base_64 = require('base-64');
Vue.use(VueAxios, axios);
// Vue.use(base_64);

Vue.component('IndexComponent', require('./components/IndexComponent.vue').default);
import IndexComponent from './components/IndexComponent.vue';
// const router = new VueRouter({ mode: 'history', routes: routes});
// const app = new Vue(Vue.util.extend({ router })).$mount('#app');
const app = new Vue({
    el: '#app',
});
