<?php

use Illuminate\Database\Seeder;

class SpotTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('spots');
        \App\Spot::insert([
            'cell_no' => 1
        ]);

        \App\Spot::insert([
            'cell_no' => 2
        ]);

        \App\Spot::insert([
            'cell_no' => 3
        ]);

        \App\Spot::insert([
            'cell_no' => 4
        ]);

        \App\Spot::insert([
            'cell_no' => 5
        ]);

        \App\Spot::insert([
            'cell_no' => 6
        ]);

        \App\Spot::insert([
            'cell_no' => 7
        ]);

        \App\Spot::insert([
            'cell_no' => 8
        ]);

        \App\Spot::insert([
            'cell_no' => 9
        ]);

        \App\Spot::insert([
            'cell_no' => 10
        ]);

        \App\Spot::insert([
            'cell_no' => 11
        ]);

        \App\Spot::insert([
            'cell_no' => 12
        ]);

        \App\Spot::insert([
            'cell_no' => 13
        ]);

        \App\Spot::insert([
            'cell_no' => 14
        ]);

        \App\Spot::insert([
            'cell_no' => 15
        ]);

        \App\Spot::insert([
            'cell_no' => 16
        ]);

        \App\Spot::insert([
            'cell_no' => 17
        ]);

        \App\Spot::insert([
            'cell_no' => 18
        ]);

        \App\Spot::insert([
            'cell_no' => 19
        ]);

        \App\Spot::insert([
            'cell_no' => 20
        ]);


    }
}
