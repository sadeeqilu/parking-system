<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehicleSpotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_spots', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('spot_id')->nullable()->unsigned();
            $table->bigInteger('vehicle_id')->nullable()->unsigned();
            $table->bigInteger('time_spent')->nullable()->default(0);
            $table->timestamps();

            $table->foreign('spot_id')
                ->references('id')
                ->on('spots');

            $table->foreign('vehicle_id')
                ->references('id')
                ->on('vehicles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_spots');
    }
}
