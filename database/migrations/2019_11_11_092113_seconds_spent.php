<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class SecondsSpent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        DB::statement('CREATE OR REPLACE VIEW seconds_per_slot AS (SELECT spots.cell_no, 
        SUM(vehicles.time_out - vehicles.time_in) as seconds_spent from
         spots,vehicles where spots.id = vehicles.spot_id GROUP BY spots.cell_no);');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('DROP VIEW seconds_per_slot');
    }
}
