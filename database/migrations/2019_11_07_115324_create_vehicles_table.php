<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVehiclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand_name')->default('');
            $table->string('plate_number')->default('');
            $table->dateTime('time_in')->default(\Carbon\Carbon::now());
            $table->dateTime('time_out')->nullable();
            $table->double('amount_paid')->nullable()->default(0.0);
            $table->bigInteger('spot_id')->unsigned()->nullable();
            $table->bigInteger('updated_spot_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('spot_id')
                ->references('id')
                ->on('spots');

            $table->foreign('updated_spot_id')
                ->references('id')
                ->on('spots');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicles');
    }
}
