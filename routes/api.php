<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// to return all parking slots and their status
Route::get('/parking-slots', 'SpotController@index');

// get all vehicles that have ever parked in the parking lot
Route::get('/vehicles', 'VehicleController@index');

//get all vehicles that are currently at the parking lot
Route::get('/get-vehicles', 'VehicleController@getVehicles');

// route to park car
Route::post('/park-car','VehicleController@parkCar');

// route to remove car from parking lot
Route::put('/car-exit/{cell_no}', 'VehicleController@exitCar');

// route to get the to 5 busiest parking spaces
Route::get('/busiest-slots','SpotController@getBusiestSlots');

// todo: pay for parking
Route::post('/pay-parking-fee', [
    'name' => 'SpotController@payParkingFee',
    'as' => 'pay-parking-fee'
]);

