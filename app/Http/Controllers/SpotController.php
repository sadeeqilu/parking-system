<?php

namespace App\Http\Controllers;

use App\Http\Resources\SpotResource;
use App\Spot;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class SpotController extends Controller
{
    private $vehicle, $spot;

    public function __construct(Vehicle $vehicle, Spot $spot)
    {
        $this->vehicle = $vehicle;
        $this->spot = $spot;
    }

    public function index(){
        return SpotResource::collection(Spot::all());
    }

    public function getBusiestSlots() {
        return response(\DB::table('seconds_per_slot')->whereNotIn('seconds_spent', [0])
            ->orderBy('seconds_spent', 'desc')->take(5)
            ->get()->jsonSerialize(), Response::HTTP_OK);
    }

}
