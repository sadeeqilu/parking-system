<?php

namespace App\Http\Controllers;

use App\Http\Resources\VehicleResource;
use App\Spot;
use App\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class VehicleController extends Controller
{
    private $vehicle,$spot;

    public function __construct(Vehicle $vehicle, Spot $spot)
    {
        $this->vehicle = $vehicle;
        $this->spot = $spot;
    }

    public function index() {
        return response(Vehicle::all()->jsonSerialize(), Response::HTTP_OK);
    }

    public function parkCar(Request $request){
        $occupySpot = $this->findSpot();
        if ($occupySpot == null)
            return response()->json([
                'message' => 'Parking lot is full',
                'type' => 'error'
            ],400);
        $data = [
            'brand_name' => $request['brand_name'],
            'plate_number' => $request['plate_number'],
            'spot_id' => $occupySpot->id
        ];

        try
        {
            $this->spot->occupySpot($occupySpot);
            $this->vehicle->parkVehicle($data);
            return response()->json([
                'type' => 'success',
                'data' => VehicleResource::collection(Vehicle::all())
            ],200);
        }catch (\Exception $exception){
            return [$exception->getMessage()];
        }

    }

    public function findSpot()
    {
        try
        {
            // when a new car comes to park, select an empty cell at random
            do
            {
                $random_cell = mt_rand(1,20);
                // check if the front spot is available and assign it to the vehicle
                if ($random_cell % 2 == 0){

                    $occupySpot = Spot::where([
                        'cell_no' => $random_cell - 1,
                        'status' => false
                    ])->get()->first();
                    if ($occupySpot != null)
                        return $occupySpot;
                }

                $occupySpot = Spot::where([
                    'cell_no' => $random_cell,
                    'status' => false
                ])->get()->first();
            }while($occupySpot->status);

            return $occupySpot ? $occupySpot : $this->findSpot();
        }catch (\Exception $exception)
        {
            if (count($this->spot->getEmptySlotsAvailable()) > 0)
                return $this->findSpot();
            else return null;
        }

    }

    public function exitCar(Request $request, $cell_no)
    {
        try
        {
            $carSpot = Spot::where([
                'cell_no' => $cell_no
            ]) ->first();

            $car = Vehicle::find($request['id']);
            if ($car->spot_id == $carSpot->id || $car->updated_spot_id == $carSpot->id)
            {
                $this->spot->exitCar($carSpot);
                $this->vehicle->vehicleExit($car);
                if ($carSpot->cell_no % 2 == 1)      // if there is a car at the front
                {

                    $id = $carSpot->cell_no + 1;
                    $statusCheck = $this->spot->checkAvailability($id);
                    if($statusCheck){               //if there is a car behind the one going out
                        $newCar = $this->vehicle->findCarInSpot($statusCheck->cell_no);
                        $this->spot->exitCar($statusCheck);
                        $newSpot = $this->findSpot();
                       $this->spot->occupySpot($newSpot);
                       $newCar['updated_spot_id'] = $newSpot->id;
                       $this->vehicle->reParkVehicle($newCar);
                   }
                }
                return VehicleResource::collection(Vehicle::all());
            }
            else return response(VehicleResource::all()->jsonSerialize(), Response::HTTP_BAD_REQUEST);


        }catch (\Exception $exception){
            return [$exception->getMessage()];
        }
    }

    public function getVehicles()
    {
        return response(\DB::table('vehicles')->where('time_out', null)
            ->get()->jsonSerialize(), Response::HTTP_OK);
    }
}
