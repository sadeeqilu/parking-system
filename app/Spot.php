<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Spot extends Model
{
    protected $fillable = [
        'cell_no', 'status'
    ];

    public function occupySpot($data)
    {
        return self::where([
            'cell_no' => $data['cell_no'],
        ])->update([
            'status' => true
        ]);
    }

    public function exitCar($data)
    {
        return self::where([
            'cell_no' => $data['cell_no'],
        ])->update([
            'status' => false
        ]);
    }

    public function getEmptySlotsAvailable()
    {
        return self::where([
            'status' => false
        ])->get();
    }

    public function checkAvailability($id) {
        return self::where([
            'cell_no' => $id,
            'status' => true
        ])->first();
    }

    public function vehicles()
    {
        return $this->hasMany('App\Vehicle');
    }
}
