<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    protected $fillable = [
        'brand_name', 'plate_number', 'time_in', 'time_out'
    ];

    public function parkVehicle($data)
    {
        return self::insert([
            'brand_name' => $data['brand_name'],
            'plate_number' => $data['plate_number'],
            'spot_id' => $data['spot_id'],
            'time_in' => Carbon::now()
        ]);
    }

    public  function vehicleExit($data) {
        return self::where([
            'id' => $data['id']
        ])->update([
            'time_out' => Carbon::now(),
            'amount_paid' => Carbon::now()->diffInSeconds($data['time_in']) * 30
        ]);
    }

    public function reParkVehicle($data) {
        return self::where([
            'id' => $data['id']
        ])->update([
            'updated_spot_id' => $data['updated_spot_id']
        ]);
    }

    public function findCarInSpot($id) {
        return self::where([
            'spot_id' => $id
        ])->orWhere('updated_spot_id',$id)->get()->first();
    }
//    public function spots()
//    {
//        return $this->belongsTo('App\Spot');
//    }

}
