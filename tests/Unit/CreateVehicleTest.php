<?php

namespace Tests\Unit;

use App\Vehicle;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CreateVehicleTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testCreateVehicle()
    {
        $data['brand_name'] = 'Mercedes';
        $data['plate_number'] = 'XA-KWT';

        $vehicle = new Vehicle();

        $this->assertTrue($vehicle->createVehicle($data));

     }
}
